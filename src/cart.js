import { createStore } from 'vuex';
import createPersistedState from "vuex-persistedstate";

export default createStore({
  plugins: [createPersistedState()],
  state: {
    cart: [], 
  },
 mutations: {
  addToCart(state, product) {
    const item = state.cart.find(cartItem => cartItem.product.id === product.id);
    if (item) {
      item.count++;
    } else {
      state.cart.push({product, count: 1})
    }
   },
   updateCartItem(state, updatedItem) {
      const index = state.cart.findIndex(item => item.product.id === updatedItem.product.id);
      if (index !== -1) {
        state.cart[index] = updatedItem;
      }
    },
  removeFromCart(state, index) {
    state.cart.splice(index, 1);
  },
},

  actions: {
  },
  getters: {
    cart: state => state.cart,
    totalCost: state => {
      return state.cart.reduce((total, cartItem) => total + cartItem.product.price * cartItem.count, 0);
    },
  },
});
